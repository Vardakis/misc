/*
 * Copyright (c) 2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <time.h>
#include <pthread.h>

#define DMA_BASE_ADDRESS     0x7E200000

#define XAXIDMA_TX_OFFSET	0x00000000 /**< TX channel registers base
					     *  offset */
#define XAXIDMA_RX_OFFSET	0x00000030 /**< RX channel registers base
					     * offset */

#define DDR_BASE_ADDRESS     0x10000000

#define DDR_BASE_WRITE_ADDRESS    0x18000000

/* This set of registers are applicable for both channels. Add
 * XAXIDMA_TX_OFFSET to get to TX channel, and XAXIDMA_RX_OFFSET to get to RX
 * channel
 */
#define XAXIDMA_CR_OFFSET	 0x00000000   /**< Channel control */
#define XAXIDMA_SR_OFFSET	 0x00000004   /**< Status */
#define XAXIDMA_CDESC_OFFSET	 0x00000008   /**< Current descriptor pointer */
#define XAXIDMA_CDESC_MSB_OFFSET 0x0000000C   /**< Current descriptor pointer */
#define XAXIDMA_TDESC_OFFSET	 0x00000010   /**< Tail descriptor pointer */
#define XAXIDMA_TDESC_MSB_OFFSET 0x00000014   /**< Tail descriptor pointer */
#define XAXIDMA_SRCADDR_OFFSET	 0x00000018   /**< Simple mode source address
						pointer */
#define XAXIDMA_SRCADDR_MSB_OFFSET	0x0000001C  /**< Simple mode source address
						pointer */
#define XAXIDMA_DESTADDR_OFFSET		0x00000018   /**< Simple mode destination address pointer */
#define XAXIDMA_DESTADDR_MSB_OFFSET	0x0000001C   /**< Simple mode destination address pointer */
#define XAXIDMA_BUFFLEN_OFFSET		0x00000028   /**< Tail descriptor pointer */
#define XAXIDMA_SGCTL_OFFSET		0x0000002c   /**< SG Control Register */

/** Multi-Channel DMA Descriptor Offsets **/
#define XAXIDMA_RX_CDESC0_OFFSET	0x00000040   /**< Rx Current Descriptor 0 */
#define XAXIDMA_RX_CDESC0_MSB_OFFSET	0x00000044   /**< Rx Current Descriptor 0 */
#define XAXIDMA_RX_TDESC0_OFFSET	0x00000048   /**< Rx Tail Descriptor 0 */
#define XAXIDMA_RX_TDESC0_MSB_OFFSET	0x0000004C   /**< Rx Tail Descriptor 0 */
#define XAXIDMA_RX_NDESC_OFFSET		0x00000020   /**< Rx Next Descriptor Offset */
/*@}*/

/** @name Bitmasks of XAXIDMA_CR_OFFSET register
 * @{
 */
#define XAXIDMA_CR_RUNSTOP_MASK	0x00000001 /**< Start/stop DMA channel */
#define XAXIDMA_CR_RESET_MASK	0x00000004 /**< Reset DMA engine */
#define XAXIDMA_CR_KEYHOLE_MASK	0x00000008 /**< Keyhole feature */
#define XAXIDMA_CR_CYCLIC_MASK	0x00000010 /**< Cyclic Mode */
/*@}*/

/** @name Bitmasks of XAXIDMA_SR_OFFSET register
 *
 * This register reports status of a DMA channel, including
 * run/stop/idle state, errors, and interrupts (note that interrupt
 * masks are shared with XAXIDMA_CR_OFFSET register, and are defined
 * in the _IRQ_ section.
 *
 * The interrupt coalescing threshold value and delay counter value are
 * also shared with XAXIDMA_CR_OFFSET register, and are defined in a
 * later section.
 * @{
 */
#define XAXIDMA_HALTED_MASK		0x00000001  /**< DMA channel halted */
#define XAXIDMA_IDLE_MASK		0x00000002  /**< DMA channel idle */
#define XAXIDMA_ERR_INTERNAL_MASK	0x00000010  /**< Datamover internal
						      *  err */
#define XAXIDMA_ERR_SLAVE_MASK		0x00000020  /**< Datamover slave err */
#define XAXIDMA_ERR_DECODE_MASK		0x00000040  /**< Datamover decode
						      *  err */
#define XAXIDMA_ERR_SG_INT_MASK		0x00000100  /**< SG internal err */
#define XAXIDMA_ERR_SG_SLV_MASK		0x00000200  /**< SG slave err */
#define XAXIDMA_ERR_SG_DEC_MASK		0x00000400  /**< SG decode err */
#define XAXIDMA_ERR_ALL_MASK		0x00000770  /**< All errors */

/** @name Bitmask for interrupts
 * These masks are shared by XAXIDMA_CR_OFFSET register and
 * XAXIDMA_SR_OFFSET register
 * @{
 */
#define XAXIDMA_IRQ_IOC_MASK		0x00001000 /**< Completion intr */
#define XAXIDMA_IRQ_DELAY_MASK		0x00002000 /**< Delay interrupt */
#define XAXIDMA_IRQ_ERROR_MASK		0x00004000 /**< Error interrupt */
#define XAXIDMA_IRQ_ALL_MASK		0x00007000 /**< All interrupts */
/*@}*/

#define XAXIDMA_RESET_TIMEOUT       500

#define MAP_SIZE 4096UL
#define MAP_MASK (MAP_SIZE - 1)

#define DDR_MAP_SIZE 0x10000000
#define DDR_MAP_MASK (DDR_MAP_SIZE - 1)

#define DDR_WRITE_OFFSET 0x10000000

#define BUFFER_BYTESIZE		100000	// Length of the buffers for DMA transfer

uint8_t tx_buf[BUFFER_BYTESIZE];
size_t offset = 0;
size_t read_offset = 0;
uint8_t started = 0;

pthread_mutex_t offset_lock, r_offset_lock;

int memfd;

#define PORT        9080
#define WS_PORT     9081
#define MAXBYTES    1472

void* rx_thread(void *vargp) {

	void *mapped_base, *dma_base;
	void *mapped_base_ddr, *ddr_base;
	off_t dev_base = DMA_BASE_ADDRESS;
	off_t ddr_write_base = DDR_BASE_ADDRESS;
	uint32_t TimeOut = XAXIDMA_RESET_TIMEOUT;
	uint32_t ResetMask, ResetMask_tx;
	uint32_t RegValue, RegValue_tx;

	mapped_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, memfd,
			dev_base & ~MAP_MASK);
	if (mapped_base == (void*) -1) {
		printf("Can't map the memory to user space.\n");
		exit(0);
	}

	dma_base = mapped_base + (dev_base & MAP_MASK);

	mapped_base_ddr = mmap(0, DDR_MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED,
			memfd, ddr_write_base & ~DDR_MAP_MASK);
	if (mapped_base_ddr == (void*) -1) {
		printf("Can't map the memory to user space.\n");
		exit(0);
	}

	ddr_base = mapped_base_ddr + (ddr_write_base & DDR_MAP_MASK);

	ResetMask = (uint32_t) XAXIDMA_CR_RESET_MASK;

	*((volatile uint32_t*) (dma_base + XAXIDMA_TX_OFFSET +
	XAXIDMA_CR_OFFSET)) = ResetMask;

	do {
		/* If the reset bit is still high, then reset is not done	*/
		ResetMask_tx = *((volatile uint32_t*) (dma_base + XAXIDMA_TX_OFFSET
				+ XAXIDMA_CR_OFFSET));

		if (!(ResetMask_tx & XAXIDMA_CR_RESET_MASK)) {
			break;
		}
		TimeOut -= 1;
	} while (TimeOut);
	if (!TimeOut){
		printf("Can't reset.\n");
		exit(0);
	}

	TimeOut = XAXIDMA_RESET_TIMEOUT;

	while (TimeOut) {
		RegValue = *((volatile uint32_t*) (dma_base + XAXIDMA_TX_OFFSET +
		XAXIDMA_CR_OFFSET));
		RegValue = (uint32_t) (RegValue | XAXIDMA_CR_RUNSTOP_MASK);
		*((volatile uint32_t*) (dma_base + XAXIDMA_TX_OFFSET +
		XAXIDMA_CR_OFFSET)) = RegValue;

		RegValue_tx = *((volatile uint32_t*) (dma_base + XAXIDMA_TX_OFFSET +
		XAXIDMA_SR_OFFSET));

		if ((RegValue_tx & XAXIDMA_HALTED_MASK)) {
			TimeOut -= 1;
		} else {
			break;
		}
	}
	if (TimeOut == 0) {
		printf("DMA engine cannot start \n");
		exit(0);
	}

	printf("TX DMA engine started \n");

	int sockfd;
	struct sockaddr_in servaddr;

	if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("socket creation failed");
		exit(EXIT_FAILURE);
	}
	struct timeval read_timeout;
	read_timeout.tv_sec = 0;
	read_timeout.tv_usec = 10;
	setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &read_timeout, sizeof read_timeout);
	memset(&servaddr, 0, sizeof(servaddr));

	servaddr.sin_family = AF_INET; // IPv4
	servaddr.sin_addr.s_addr = INADDR_ANY;
	servaddr.sin_port = htons(PORT);

	if (bind(sockfd, (const struct sockaddr*) &servaddr, sizeof(servaddr))
			< 0) {
		perror("bind failed");
		exit(EXIT_FAILURE);
	}
	int len;
	uint16_t n;
	len = sizeof(servaddr);
	uint8_t opt = 0;
	int copied = 0;

	while (1) {
		RegValue_tx = *((volatile uint32_t*) (dma_base + XAXIDMA_TX_OFFSET +
		XAXIDMA_SR_OFFSET));
		RegValue = *((volatile uint32_t*) (dma_base + XAXIDMA_TX_OFFSET +
				XAXIDMA_CR_OFFSET));
		printf("TX : CR = %d, SR = %d \n",RegValue, RegValue_tx);
		n = recvfrom(sockfd, (uint8_t*) ddr_base, MAXBYTES, 0,
				(struct sockaddr*) &servaddr, &len);
		if(n == 65535){
			continue;
		}

		offset = 1;

		*((volatile uint32_t*) (dma_base + XAXIDMA_TX_OFFSET
				+ XAXIDMA_SRCADDR_OFFSET)) = (uint32_t) DDR_BASE_ADDRESS
		;
		*((volatile uint32_t*) (dma_base + XAXIDMA_TX_OFFSET
				+ XAXIDMA_BUFFLEN_OFFSET)) = (uint32_t) n;

		RegValue_tx = *((volatile uint32_t*) (dma_base + XAXIDMA_TX_OFFSET +
		XAXIDMA_SR_OFFSET));
		while (!(RegValue_tx & XAXIDMA_IRQ_ALL_MASK)) {
			RegValue_tx = *((volatile uint32_t*) (dma_base + XAXIDMA_TX_OFFSET +
			XAXIDMA_SR_OFFSET));
			RegValue = *((volatile uint32_t*) (dma_base + XAXIDMA_TX_OFFSET +
					XAXIDMA_CR_OFFSET));
			printf("n = %d, SR = %d \n",n, RegValue_tx);
			usleep(1);
		}
		if (!(RegValue_tx & XAXIDMA_IRQ_IOC_MASK)) {
			printf("Error interrupt. %d Dropping packet \n", RegValue_tx);
			*((volatile uint32_t*) (dma_base + XAXIDMA_TX_OFFSET
					+ XAXIDMA_SR_OFFSET)) = (*((volatile uint32_t*) (dma_base
					+ XAXIDMA_TX_OFFSET + XAXIDMA_SR_OFFSET)) |
			XAXIDMA_IRQ_ALL_MASK);
			continue;
		}
		//printf("2. DMA TX\n");
		*((volatile uint32_t*) (dma_base + XAXIDMA_TX_OFFSET + XAXIDMA_SR_OFFSET)) =
				(*((volatile uint32_t*) (dma_base + XAXIDMA_TX_OFFSET
						+ XAXIDMA_SR_OFFSET)) |
				XAXIDMA_IRQ_ALL_MASK);

	}
}

void* tx_thread(void *vargp) {
	void *mapped_base, *dma_base;
	void *mapped_base_ddr, *ddr_base;
	off_t dev_base = DMA_BASE_ADDRESS;
	off_t ddr_write_base = DDR_BASE_WRITE_ADDRESS;
	uint32_t TimeOut = XAXIDMA_RESET_TIMEOUT;
	uint32_t ResetMask, ResetMask_rx;
	uint32_t RegValue, RegValue_rx;

	mapped_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, memfd,
			dev_base & ~MAP_MASK);
	if (mapped_base == (void*) -1) {
		printf("Can't map the memory to user space.\n");
		exit(0);
	}

	dma_base = mapped_base + (dev_base & MAP_MASK);

	mapped_base_ddr = mmap(0, DDR_MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED,
			memfd, ddr_write_base & ~DDR_MAP_MASK);
	if (mapped_base_ddr == (void*) -1) {
		printf("Can't map the memory to user space.\n");
		exit(0);
	}

	ddr_base = mapped_base_ddr + (ddr_write_base & DDR_MAP_MASK);

	ResetMask = (uint32_t) XAXIDMA_CR_RESET_MASK;

	*((volatile uint32_t*) (dma_base + XAXIDMA_RX_OFFSET +
	XAXIDMA_CR_OFFSET)) = ResetMask;

	do {
		/* If the reset bit is still high, then reset is not done	*/
		ResetMask_rx = *((volatile uint32_t*) (dma_base + XAXIDMA_RX_OFFSET
				+ XAXIDMA_CR_OFFSET));

		if (!(ResetMask_rx & XAXIDMA_CR_RESET_MASK)) {
			break;
		}
		TimeOut -= 1;
	} while (TimeOut);

	TimeOut = XAXIDMA_RESET_TIMEOUT;

	while (TimeOut) {
		RegValue = *((volatile uint32_t*) (dma_base + XAXIDMA_RX_OFFSET +
		XAXIDMA_CR_OFFSET));
		RegValue = (uint32_t) (RegValue | XAXIDMA_CR_RUNSTOP_MASK);
		*((volatile uint32_t*) (dma_base + XAXIDMA_RX_OFFSET +
		XAXIDMA_CR_OFFSET)) = RegValue;

		RegValue_rx = *((volatile uint32_t*) (dma_base + XAXIDMA_RX_OFFSET +
		XAXIDMA_SR_OFFSET));

		if ((RegValue_rx & XAXIDMA_HALTED_MASK)) {
			TimeOut -= 1;
		} else {
			break;
		}
	}
	if (TimeOut == 0) {
		printf("DMA engine cannot start \n");
		exit(0);
	}
	TimeOut = XAXIDMA_RESET_TIMEOUT;

	printf("RX DMA engine started \n");

	int sockfd;
	struct sockaddr_in cliaddr;
	if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("socket creation failed");
		exit(EXIT_FAILURE);
	}
	memset(&cliaddr, 0, sizeof(cliaddr));

	cliaddr.sin_family = AF_INET; // IPv4
	cliaddr.sin_addr.s_addr = inet_addr("147.52.17.89");
	cliaddr.sin_port = htons(WS_PORT);
	*((volatile uint32_t*) (dma_base + XAXIDMA_RX_OFFSET
			+ XAXIDMA_DESTADDR_OFFSET)) = (uint32_t) DDR_BASE_WRITE_ADDRESS;
	*((volatile uint32_t*) (dma_base + XAXIDMA_RX_OFFSET
			+ XAXIDMA_BUFFLEN_OFFSET)) = (uint32_t) MAXBYTES;
	while (!offset) {
		RegValue_rx = *((volatile uint32_t*) (dma_base + XAXIDMA_RX_OFFSET +
		XAXIDMA_SR_OFFSET));
		RegValue = *((volatile uint32_t*) (dma_base + XAXIDMA_RX_OFFSET +
				XAXIDMA_CR_OFFSET));
		printf("RX: CR = %d, SR = %d \n",RegValue, RegValue_rx);
		usleep(10);
	}
	RegValue_rx = *((volatile uint32_t*) (dma_base + XAXIDMA_RX_OFFSET +
			XAXIDMA_SR_OFFSET));

	printf("Pass %d\n", RegValue_rx);
	started = 1;
	uint16_t length = 0;
	int len;
	len = sizeof(cliaddr);
	uint16_t txed = 0;
	while (1) {
		RegValue_rx = *((volatile uint32_t*) (dma_base + XAXIDMA_RX_OFFSET +
		XAXIDMA_SR_OFFSET));
		if(!(RegValue_rx & XAXIDMA_IDLE_MASK)){
			usleep(10);
			continue;
		}
		printf("Pass\n");
		*((volatile uint32_t*) (dma_base + XAXIDMA_RX_OFFSET
				+ XAXIDMA_DESTADDR_OFFSET)) = (uint32_t) DDR_BASE_WRITE_ADDRESS;
		*((volatile uint32_t*) (dma_base + XAXIDMA_RX_OFFSET
				+ XAXIDMA_BUFFLEN_OFFSET)) = (uint32_t) MAXBYTES;


		RegValue_rx = *((volatile uint32_t*) (dma_base
				+ XAXIDMA_RX_OFFSET +
				XAXIDMA_SR_OFFSET));
		while (!(RegValue_rx & XAXIDMA_IRQ_ALL_MASK) && (TimeOut)) {
			RegValue_rx = *((volatile uint32_t*) (dma_base
					+ XAXIDMA_RX_OFFSET +
					XAXIDMA_SR_OFFSET));
			printf("4= %d ", RegValue_rx);
			TimeOut -= 1;
		}
		if(!TimeOut){
			TimeOut = XAXIDMA_RESET_TIMEOUT;
			continue;
		}
		if ((RegValue_rx & XAXIDMA_IRQ_ERROR_MASK)) {
			printf("Error interrupt %d.  Dropping packet \n", RegValue_rx);
			*((volatile uint32_t*) (dma_base + XAXIDMA_RX_OFFSET
					+ XAXIDMA_SR_OFFSET)) =
					(*((volatile uint32_t*) (dma_base + XAXIDMA_RX_OFFSET
							+ XAXIDMA_SR_OFFSET)) |
					XAXIDMA_IRQ_ALL_MASK);
			continue;
		}
		*((volatile uint32_t*) (dma_base + XAXIDMA_RX_OFFSET
				+ XAXIDMA_SR_OFFSET)) = (*((volatile uint32_t*) (dma_base
				+ XAXIDMA_RX_OFFSET + XAXIDMA_SR_OFFSET)) |
		XAXIDMA_IRQ_ALL_MASK);
		length = *((volatile uint32_t*) (dma_base + XAXIDMA_RX_OFFSET
				+ XAXIDMA_BUFFLEN_OFFSET));

		sendto(sockfd, (uint8_t *)ddr_base, length, 0,
				(const struct sockaddr*) &cliaddr, len);

		printf("TX %d\n", length);
	}
}

int main() {
	memfd = open("/dev/mem", O_RDWR | O_SYNC);
	if (memfd == -1) {
		printf("Can't open /dev/mem.\n");
		exit(0);
	}
	pthread_t rx, tx;
	pthread_create(&tx, NULL, tx_thread, (void*) &tx);
	sleep(2);
	pthread_create(&rx, NULL, rx_thread, (void*) &rx);


	pthread_exit(NULL);
	return 0;
}
