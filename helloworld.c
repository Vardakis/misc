/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"


#include "xaxidma_hw.h"

#define DDR4_BASE      0x400000000
#define DDR4_BASE_LOW  0x00000000
#define DDR4_BASE_HIGH 0x00000004
#define BD_OFFSET      0x40

int
mm2s_stop(){
	uint32_t Regvalue = Xil_In32(XPAR_AXI_DMA_0_BASEADDR + XAXIDMA_TX_OFFSET +
	XAXIDMA_CR_OFFSET);
	Regvalue = (uint32_t)(Regvalue & ~XAXIDMA_CR_RUNSTOP_MASK);
	Xil_Out32(XPAR_AXI_DMA_0_BASEADDR + XAXIDMA_TX_OFFSET +
			XAXIDMA_CR_OFFSET, Regvalue);
	return 0;
}

int
s2mm_stop(){
	uint32_t Regvalue = Xil_In32(XPAR_AXI_DMA_0_BASEADDR + XAXIDMA_RX_OFFSET +
	XAXIDMA_CR_OFFSET);
	Regvalue = (uint32_t)(Regvalue & ~XAXIDMA_CR_RUNSTOP_MASK);
	Xil_Out32(XPAR_AXI_DMA_0_BASEADDR + XAXIDMA_RX_OFFSET +
			XAXIDMA_CR_OFFSET, Regvalue);
	return 0;
}

int main()
{
    init_platform();
    uint32_t val;
    val = Xil_In32(XPAR_AXI_DMA_0_BASEADDR + XAXIDMA_TX_OFFSET + XAXIDMA_CR_OFFSET);
    Xil_Out32(XPAR_AXI_DMA_0_BASEADDR + XAXIDMA_TX_OFFSET + XAXIDMA_CR_OFFSET, val | XAXIDMA_CR_RESET_MASK);

    while(((Xil_In32(XPAR_AXI_DMA_0_BASEADDR + XAXIDMA_TX_OFFSET + XAXIDMA_CR_OFFSET)) & XAXIDMA_CR_RESET_MASK)){

    }
    size_t len = 1024;
    for(int i = 0 ; i < len; i++){
    	Xil_Out32(0x10000000 + i * 0x04, i);
    }

//    Xil_Out32(DDR4_BASE, DDR4_BASE_LOW);
//    Xil_Out32(DDR4_BASE + XAXIDMA_BD_NDESC_MSB_OFFSET, DDR4_BASE_HIGH);
//    Xil_Out32(DDR4_BASE + XAXIDMA_BD_BUFA_OFFSET, 0x10000000);
//    Xil_Out32(DDR4_BASE + XAXIDMA_BD_BUFA_MSB_OFFSET, 0x00000000);
//    Xil_Out32(DDR4_BASE + XAXIDMA_BD_CTRL_LEN_OFFSET, XAXIDMA_BD_CTRL_ALL_MASK | len);
//    Xil_Out32(DDR4_BASE + XAXIDMA_BD_STS_OFFSET, 0);
//
//    Xil_Out32(DDR4_BASE + BD_OFFSET, DDR4_BASE_LOW + BD_OFFSET);
//    Xil_Out32(DDR4_BASE + BD_OFFSET +XAXIDMA_BD_NDESC_MSB_OFFSET, DDR4_BASE_HIGH);
//    Xil_Out32(DDR4_BASE + BD_OFFSET + XAXIDMA_BD_BUFA_OFFSET, 0x80000000);
//    Xil_Out32(DDR4_BASE + BD_OFFSET + XAXIDMA_BD_BUFA_MSB_OFFSET, 0x00000000);
//    Xil_Out32(DDR4_BASE + XAXIDMA_BD_STS_OFFSET + BD_OFFSET, 0);
//    Xil_Out32(DDR4_BASE + BD_OFFSET + XAXIDMA_BD_CTRL_LEN_OFFSET, len);
//
//    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_RX_OFFSET + XAXIDMA_CDESC_OFFSET, DDR4_BASE_LOW + BD_OFFSET);
//    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_RX_OFFSET + XAXIDMA_CDESC_MSB_OFFSET, DDR4_BASE_HIGH);
//    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_RX_OFFSET + XAXIDMA_CR_OFFSET, XAXIDMA_CR_RUNSTOP_MASK);
//    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_RX_OFFSET + XAXIDMA_TDESC_OFFSET, DDR4_BASE_LOW + BD_OFFSET);
//    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_RX_OFFSET + XAXIDMA_TDESC_MSB_OFFSET, DDR4_BASE_HIGH);
//
//    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_CDESC_OFFSET, DDR4_BASE_LOW);
//    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_CDESC_MSB_OFFSET, DDR4_BASE_HIGH);
//    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_CR_OFFSET, XAXIDMA_CR_RUNSTOP_MASK);
//    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_TDESC_OFFSET, DDR4_BASE_LOW);
//    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_TDESC_MSB_OFFSET, DDR4_BASE_HIGH);
//
//    while(!((Xil_In32(DDR4_BASE + 0x1C) & XAXIDMA_BD_STS_COMPLETE_MASK))){
//
//    }
//    mm2s_stop();
//
//    while(!((Xil_In32(DDR4_BASE + BD_OFFSET + 0x1C) & XAXIDMA_BD_STS_COMPLETE_MASK))){
//
//    }
//    s2mm_stop();

    Xil_Out32(DDR4_BASE, DDR4_BASE_LOW);
    Xil_Out32(DDR4_BASE + XAXIDMA_BD_NDESC_MSB_OFFSET, DDR4_BASE_HIGH);
    Xil_Out32(DDR4_BASE + XAXIDMA_BD_BUFA_OFFSET, 0x80000000);
    Xil_Out32(DDR4_BASE + XAXIDMA_BD_BUFA_MSB_OFFSET, 0x00000000);
    Xil_Out32(DDR4_BASE + XAXIDMA_BD_CTRL_LEN_OFFSET, XAXIDMA_BD_CTRL_ALL_MASK | len);
    Xil_Out32(DDR4_BASE + XAXIDMA_BD_STS_OFFSET, 0);

    Xil_Out32(DDR4_BASE + BD_OFFSET, DDR4_BASE_LOW + BD_OFFSET);
    Xil_Out32(DDR4_BASE + BD_OFFSET +XAXIDMA_BD_NDESC_MSB_OFFSET, DDR4_BASE_HIGH);
    Xil_Out32(DDR4_BASE + BD_OFFSET + XAXIDMA_BD_BUFA_OFFSET, 0x18000000);
    Xil_Out32(DDR4_BASE + BD_OFFSET + XAXIDMA_BD_BUFA_MSB_OFFSET, 0x00000000);
    Xil_Out32(DDR4_BASE + XAXIDMA_BD_STS_OFFSET + BD_OFFSET, 0);
    Xil_Out32(DDR4_BASE + BD_OFFSET + XAXIDMA_BD_CTRL_LEN_OFFSET, len);

    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_RX_OFFSET + XAXIDMA_CDESC_OFFSET, DDR4_BASE_LOW + BD_OFFSET);
    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_RX_OFFSET + XAXIDMA_CDESC_MSB_OFFSET, DDR4_BASE_HIGH);
    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_RX_OFFSET + XAXIDMA_CR_OFFSET, XAXIDMA_CR_RUNSTOP_MASK);
    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_RX_OFFSET + XAXIDMA_TDESC_OFFSET, DDR4_BASE_LOW + BD_OFFSET);
    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_RX_OFFSET + XAXIDMA_TDESC_MSB_OFFSET, DDR4_BASE_HIGH);

    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_CDESC_OFFSET, DDR4_BASE_LOW);
    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_CDESC_MSB_OFFSET, DDR4_BASE_HIGH);
    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_CR_OFFSET, XAXIDMA_CR_RUNSTOP_MASK);
    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_TDESC_OFFSET, DDR4_BASE_LOW);
    Xil_Out32(XPAR_AXIDMA_0_BASEADDR + XAXIDMA_TDESC_MSB_OFFSET, DDR4_BASE_HIGH);
    cleanup_platform();
    return 0;
}
