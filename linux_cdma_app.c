

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <time.h>
#include <pthread.h>


#define DMA_BASE_ADDRESS     0x7E200000

#define XAXIDMA_TX_OFFSET	0x00000000 /**< TX channel registers base
					     *  offset */
#define XAXIDMA_RX_OFFSET	0x00000030 /**< RX channel registers base
					     * offset */

#define DDR_BASE_ADDRESS     0x10000000

#define DDR_BASE_WRITE_ADDRESS    0x18000000

/* This set of registers are applicable for both channels. Add
 * XAXIDMA_TX_OFFSET to get to TX channel, and XAXIDMA_RX_OFFSET to get to RX
 * channel
 */
#define XAXIDMA_CR_OFFSET	 0x00000000   /**< Channel control */
#define XAXIDMA_SR_OFFSET	 0x00000004   /**< Status */
#define XAXIDMA_CDESC_OFFSET	 0x00000008   /**< Current descriptor pointer */
#define XAXIDMA_CDESC_MSB_OFFSET 0x0000000C   /**< Current descriptor pointer */
#define XAXIDMA_TDESC_OFFSET	 0x00000010   /**< Tail descriptor pointer */
#define XAXIDMA_TDESC_MSB_OFFSET 0x00000014   /**< Tail descriptor pointer */
#define XAXIDMA_SRCADDR_OFFSET	 0x00000018   /**< Simple mode source address
						pointer */
#define XAXIDMA_SRCADDR_MSB_OFFSET	0x0000001C  /**< Simple mode source address
						pointer */
#define XAXIDMA_DESTADDR_OFFSET		0x00000018   /**< Simple mode destination address pointer */
#define XAXIDMA_DESTADDR_MSB_OFFSET	0x0000001C   /**< Simple mode destination address pointer */
#define XAXIDMA_BUFFLEN_OFFSET		0x00000028   /**< Tail descriptor pointer */
#define XAXIDMA_SGCTL_OFFSET		0x0000002c   /**< SG Control Register */

/** Multi-Channel DMA Descriptor Offsets **/
#define XAXIDMA_RX_CDESC0_OFFSET	0x00000040   /**< Rx Current Descriptor 0 */
#define XAXIDMA_RX_CDESC0_MSB_OFFSET	0x00000044   /**< Rx Current Descriptor 0 */
#define XAXIDMA_RX_TDESC0_OFFSET	0x00000048   /**< Rx Tail Descriptor 0 */
#define XAXIDMA_RX_TDESC0_MSB_OFFSET	0x0000004C   /**< Rx Tail Descriptor 0 */
#define XAXIDMA_RX_NDESC_OFFSET		0x00000020   /**< Rx Next Descriptor Offset */
/*@}*/

/** @name Bitmasks of XAXIDMA_CR_OFFSET register
 * @{
 */
#define XAXIDMA_CR_RUNSTOP_MASK	0x00000001 /**< Start/stop DMA channel */
#define XAXIDMA_CR_RESET_MASK	0x00000004 /**< Reset DMA engine */
#define XAXIDMA_CR_KEYHOLE_MASK	0x00000008 /**< Keyhole feature */
#define XAXIDMA_CR_CYCLIC_MASK	0x00000010 /**< Cyclic Mode */
/*@}*/

/** @name Bitmasks of XAXIDMA_SR_OFFSET register
 *
 * This register reports status of a DMA channel, including
 * run/stop/idle state, errors, and interrupts (note that interrupt
 * masks are shared with XAXIDMA_CR_OFFSET register, and are defined
 * in the _IRQ_ section.
 *
 * The interrupt coalescing threshold value and delay counter value are
 * also shared with XAXIDMA_CR_OFFSET register, and are defined in a
 * later section.
 * @{
 */
#define XAXIDMA_HALTED_MASK		0x00000001  /**< DMA channel halted */
#define XAXIDMA_IDLE_MASK		0x00000002  /**< DMA channel idle */
#define XAXIDMA_ERR_INTERNAL_MASK	0x00000010  /**< Datamover internal
						      *  err */
#define XAXIDMA_ERR_SLAVE_MASK		0x00000020  /**< Datamover slave err */
#define XAXIDMA_ERR_DECODE_MASK		0x00000040  /**< Datamover decode
						      *  err */
#define XAXIDMA_ERR_SG_INT_MASK		0x00000100  /**< SG internal err */
#define XAXIDMA_ERR_SG_SLV_MASK		0x00000200  /**< SG slave err */
#define XAXIDMA_ERR_SG_DEC_MASK		0x00000400  /**< SG decode err */
#define XAXIDMA_ERR_ALL_MASK		0x00000770  /**< All errors */

/** @name Bitmask for interrupts
 * These masks are shared by XAXIDMA_CR_OFFSET register and
 * XAXIDMA_SR_OFFSET register
 * @{
 */
#define XAXIDMA_IRQ_IOC_MASK		0x00001000 /**< Completion intr */
#define XAXIDMA_IRQ_DELAY_MASK		0x00002000 /**< Delay interrupt */
#define XAXIDMA_IRQ_ERROR_MASK		0x00004000 /**< Error interrupt */
#define XAXIDMA_IRQ_ALL_MASK		0x00007000 /**< All interrupts */
/*@}*/

#define XAXIDMA_RESET_TIMEOUT       500

#define MAP_SIZE 4096UL
#define MAP_MASK (MAP_SIZE - 1)

#define DDR_MAP_SIZE 0x10000000
#define DDR_MAP_MASK (DDR_MAP_SIZE - 1)

#define DDR_WRITE_OFFSET 0x10000000


#define BUFFER_BYTESIZE		100000	// Length of the buffers for DMA transfer

#define CTRL_PORT        8080
#define PORT 9080
#define WS_PORT   9081

#define MAXBYTES    1472

int
main()
{
	int memfd;
	void *mapped_base, *mapped_dev_base;
	off_t dev_base = DMA_BASE_ADDRESS;

	int memfd_1;
	void *mapped_base_1, *mapped_dev_base_1;
	off_t dev_base_1 = DDR_BASE_ADDRESS;

	int memfd_2;
	void *mapped_base_2, *mapped_dev_base_2;
	off_t dev_base_2 = DDR_BASE_WRITE_ADDRESS;

	uint32_t TimeOut = XAXIDMA_RESET_TIMEOUT;
	uint32_t ResetMask, ResetMask_tx, ResetMask_rx;
	uint32_t RegValue, RegValue_tx, RegValue_rx;
	uint8_t  recv_buffer[MAXBYTES];
	uint32_t Index;

	int sockfd;
	uint8_t buffer[MAXBYTES];
	struct sockaddr_in servaddr, cliaddr, respaddr;

	uint8_t tx_buf[MAXBYTES];
	uint8_t rx_buf[MAXBYTES];

	struct timespec start, stop;



	memfd = open("/dev/mem", O_RDWR | O_SYNC);
	if (memfd == -1) {
		printf("Can't open /dev/mem.\n");
		exit(0);
	}

	mapped_base_1 = mmap(0, DDR_MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED,
	                     memfd, dev_base_1 & ~DDR_MAP_MASK);
	if (mapped_base_1 == (void *) -1) {
		printf("Can't map the memory to user space.\n");
		exit(0);
	}

	mapped_dev_base_1 = mapped_base_1 + (dev_base_1 & DDR_MAP_MASK);

	memset(mapped_dev_base_1, 0, 100000);

	mapped_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, memfd,
	                   dev_base & ~MAP_MASK);
	if (mapped_base == (void *) -1) {
		printf("Can't map the memory to user space.\n");
		exit(0);
	}

	mapped_dev_base = mapped_base + (dev_base & MAP_MASK);

	mapped_base_2 = mmap(0, DDR_MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED,
	                     memfd, dev_base_2 & ~DDR_MAP_MASK);
	if (mapped_base_2 == (void *) -1) {
		printf("Can't map the memory to user space.\n");
		exit(0);
	}

	mapped_dev_base_2 = mapped_base_2 + (dev_base_2 & DDR_MAP_MASK);

	memcpy(mapped_dev_base_1, tx_buf, MAXBYTES);

	// Creating socket file descriptor
	if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("socket creation failed");
		exit(EXIT_FAILURE);
	}

	memset(&servaddr, 0, sizeof(servaddr));
	memset(&cliaddr, 0, sizeof(cliaddr));
	memset(&respaddr, 0, sizeof(respaddr));

	// Filling server information
	servaddr.sin_family    = AF_INET; // IPv4
	servaddr.sin_addr.s_addr = INADDR_ANY;
	servaddr.sin_port = htons(PORT);


	if (bind(sockfd, (const struct sockaddr *)&servaddr,
	         sizeof(servaddr)) < 0) {
		perror("bind failed");
		exit(EXIT_FAILURE);
	}

	int len, n;

	len = sizeof(cliaddr);  //len is value/resuslt

	/* ************************************************** *
	 * Reset the MM2S and S2MM DMA engines                *
	 * ************************************************** */

	ResetMask = (uint32_t)XAXIDMA_CR_RESET_MASK;

	*((volatile uint32_t *)(mapped_dev_base + XAXIDMA_TX_OFFSET +
	                        XAXIDMA_CR_OFFSET)) =
	                                ResetMask;

	*((volatile uint32_t *)(mapped_dev_base + XAXIDMA_RX_OFFSET +
	                        XAXIDMA_CR_OFFSET)) =
	                                ResetMask;
	do {
		/* If the reset bit is still high, then reset is not done	*/
		ResetMask_tx = *((volatile uint32_t *)(mapped_dev_base + XAXIDMA_TX_OFFSET
		                                       + XAXIDMA_CR_OFFSET));

		ResetMask_rx = *((volatile uint32_t *)(mapped_dev_base + XAXIDMA_RX_OFFSET
		                                       + XAXIDMA_CR_OFFSET));

		if (!(ResetMask_tx & XAXIDMA_CR_RESET_MASK)
		    && !(ResetMask_rx & XAXIDMA_CR_RESET_MASK)) {
			break;
		}
		TimeOut -= 1;
	} while (TimeOut);

	TimeOut = XAXIDMA_RESET_TIMEOUT;

	while (TimeOut) {
		RegValue = *((volatile uint32_t *)(mapped_dev_base + XAXIDMA_TX_OFFSET +
		                                   XAXIDMA_CR_OFFSET));
		RegValue = (uint32_t)(RegValue | XAXIDMA_CR_RUNSTOP_MASK);
		*((volatile uint32_t *)(mapped_dev_base + XAXIDMA_TX_OFFSET +
		                        XAXIDMA_CR_OFFSET)) = RegValue;

		RegValue = *((volatile uint32_t *)(mapped_dev_base + XAXIDMA_RX_OFFSET +
		                                   XAXIDMA_CR_OFFSET));
		RegValue = (uint32_t)(RegValue | XAXIDMA_CR_RUNSTOP_MASK);
		*((volatile uint32_t *)(mapped_dev_base + XAXIDMA_RX_OFFSET +
		                        XAXIDMA_CR_OFFSET)) = RegValue;

		RegValue_rx = *((volatile uint32_t *)(mapped_dev_base + XAXIDMA_RX_OFFSET +
		                                      XAXIDMA_SR_OFFSET));
		RegValue_tx = *((volatile uint32_t *)(mapped_dev_base + XAXIDMA_TX_OFFSET +
		                                      XAXIDMA_SR_OFFSET));

		if ((RegValue_rx & XAXIDMA_HALTED_MASK)
		    || (RegValue_tx & XAXIDMA_HALTED_MASK)){
			TimeOut -= 1;
		}
		else{
			break;
		}

	}
	if (TimeOut == 0) {
		printf("DMA engine cannot start \n");
		exit(0);
	}
	printf("DMA engine started \n");
//	/*Enable interrupts */
//	RegValue = *((volatile uint32_t *)(mapped_dev_base + XAXIDMA_TX_OFFSET +
//            XAXIDMA_CR_OFFSET));
//	RegValue = RegValue | XAXIDMA_IRQ_ALL_MASK;
//	*((volatile uint32_t *)(mapped_dev_base + XAXIDMA_TX_OFFSET +
//	            XAXIDMA_CR_OFFSET)) = (uint32_t) RegValue;
//
//	RegValue = *((volatile uint32_t *)(mapped_dev_base + XAXIDMA_RX_OFFSET +
//            XAXIDMA_CR_OFFSET));
//	RegValue = RegValue | XAXIDMA_IRQ_ALL_MASK;
//	*((volatile uint32_t *)(mapped_dev_base + XAXIDMA_RX_OFFSET +
//	            XAXIDMA_CR_OFFSET)) = (uint32_t) RegValue;
//
//	printf("Interrupts enabled \n");


	RegValue_rx = *((volatile uint32_t *)(mapped_dev_base + XAXIDMA_RX_OFFSET +
	                                      XAXIDMA_SR_OFFSET));
	while(RegValue_rx & XAXIDMA_IDLE_MASK){
		RegValue_rx = *((volatile uint32_t *)(mapped_dev_base + XAXIDMA_RX_OFFSET +
		                                      XAXIDMA_SR_OFFSET));
	}

	*((volatile uint32_t *)(mapped_dev_base + XAXIDMA_RX_OFFSET + XAXIDMA_DESTADDR_OFFSET)) =
	        (uint32_t)DDR_BASE_WRITE_ADDRESS;
	*((volatile uint32_t *)(mapped_dev_base + XAXIDMA_RX_OFFSET + XAXIDMA_BUFFLEN_OFFSET)) =
	        (uint32_t)MAXBYTES;

	printf("RX setup %d \n", rand());
	int same, nsame;
	n = 1472;
	int offset = 4;

	RegValue_tx = *((volatile uint32_t*) (mapped_dev_base + XAXIDMA_TX_OFFSET +
	XAXIDMA_SR_OFFSET));
	RegValue = *((volatile uint32_t*) (mapped_dev_base + XAXIDMA_TX_OFFSET +
			XAXIDMA_CR_OFFSET));
	printf("TX : CR = %d, SR = %d \n",RegValue, RegValue_tx);

	RegValue_tx = *((volatile uint32_t*) (mapped_dev_base + XAXIDMA_RX_OFFSET +
	XAXIDMA_SR_OFFSET));
	RegValue = *((volatile uint32_t*) (mapped_dev_base + XAXIDMA_RX_OFFSET +
			XAXIDMA_CR_OFFSET));
	printf("RX : CR = %d, SR = %d \n",RegValue, RegValue_tx);

	while (1) {
//		n-=20;
//		for(int i=0 ; i < n; i++){
//			buffer[i] = i;
//		}

		n = recvfrom(sockfd, buffer, MAXBYTES,
		             0, (struct sockaddr *) &cliaddr,
		             &len);

		if(clock_gettime(CLOCK_MONOTONIC, &start) == -1){
			exit(0);
		}

		memcpy(mapped_dev_base_1, buffer, n);
		RegValue_rx = *((volatile uint32_t *)(mapped_dev_base + XAXIDMA_RX_OFFSET +
		                                      XAXIDMA_SR_OFFSET));
		RegValue_tx = *((volatile uint32_t *)(mapped_dev_base + XAXIDMA_TX_OFFSET +
		                                      XAXIDMA_SR_OFFSET));
//		printf("TX SR = %d, RX SR = %d \n",RegValue_tx, RegValue_rx);

		*((volatile uint32_t *)(mapped_dev_base + XAXIDMA_TX_OFFSET + XAXIDMA_SRCADDR_OFFSET)) =
		        (uint32_t)DDR_BASE_ADDRESS;
		*((volatile uint32_t *)(mapped_dev_base + XAXIDMA_TX_OFFSET + XAXIDMA_BUFFLEN_OFFSET)) =
		        (uint32_t)MAXBYTES;
		offset +=4;

		RegValue_tx = *((volatile uint32_t *)(mapped_dev_base + XAXIDMA_TX_OFFSET +
					                                      XAXIDMA_SR_OFFSET));
		while(!(RegValue_tx & XAXIDMA_IRQ_ALL_MASK)){
			RegValue_tx = *((volatile uint32_t *)(mapped_dev_base + XAXIDMA_TX_OFFSET +
								                                      XAXIDMA_SR_OFFSET));
		//	printf("P2 %d ", RegValue_tx);
		}
		if(!(RegValue_tx & XAXIDMA_IRQ_IOC_MASK)){
			printf("Error interrupt. Dropping packet \n");
			continue;
		}
		*((volatile uint32_t *)(mapped_dev_base + XAXIDMA_TX_OFFSET + XAXIDMA_SR_OFFSET)) =
				(*((volatile uint32_t *)(mapped_dev_base + XAXIDMA_TX_OFFSET + XAXIDMA_SR_OFFSET)) |
				XAXIDMA_IRQ_ALL_MASK);



		*((volatile uint32_t *)(mapped_dev_base + XAXIDMA_RX_OFFSET + XAXIDMA_DESTADDR_OFFSET)) =
		        (uint32_t)DDR_BASE_WRITE_ADDRESS+ offset;
		*((volatile uint32_t *)(mapped_dev_base + XAXIDMA_RX_OFFSET + XAXIDMA_BUFFLEN_OFFSET)) =
		        (uint32_t)MAXBYTES;


		RegValue_rx = *((volatile uint32_t *)(mapped_dev_base + XAXIDMA_RX_OFFSET +
					                                      XAXIDMA_SR_OFFSET));
		while(!(RegValue_rx & XAXIDMA_IRQ_ALL_MASK)){
			RegValue_rx = *((volatile uint32_t *)(mapped_dev_base + XAXIDMA_RX_OFFSET +
								                                      XAXIDMA_SR_OFFSET));
			//printf("4 ");
		}
		if((RegValue_rx & XAXIDMA_IRQ_ERROR_MASK)){
			printf("Error interrupt. Dropping packet \n");
			continue;
		}
		*((volatile uint32_t *)(mapped_dev_base + XAXIDMA_RX_OFFSET + XAXIDMA_SR_OFFSET)) =
						(*((volatile uint32_t *)(mapped_dev_base + XAXIDMA_RX_OFFSET + XAXIDMA_SR_OFFSET)) |
						XAXIDMA_IRQ_ALL_MASK);

		memcpy(recv_buffer, mapped_dev_base_2, n);
		same = 0;
		nsame = 0;
//		memcpy(rx_buf, mapped_dev_base_2, MAXBYTES);
//		for(int i = 0; i< n; i++){
//			if(recv_buffer[i] != buffer[i]){
//				nsame++;
//			}
//			else{
//				same++;
//			}
//		}
//		printf("Same %d, not same %d, total %d \n", same, nsame, n);
//		memset(rx_buf, 0, MAXBYTES);

		memset(mapped_dev_base_2, 0, MAXBYTES);

		cliaddr.sin_port = htons(WS_PORT);

		sendto(sockfd, recv_buffer, n,
		       0, (const struct sockaddr *) &cliaddr,
		       len);

	}

	if (munmap(mapped_base_1, DDR_MAP_SIZE) == -1) {
		printf("Can't unmap memory from user space.\n");
		exit(0);
	}


	if (munmap(mapped_base, MAP_SIZE) == -1) {
		printf("Can't unmap memory from user space.\n");
		exit(0);
	}

	if (munmap(mapped_base_2, DDR_MAP_SIZE) == -1) {
		printf("Can't unmap memory from user space.\n");
		exit(0);
	}

	close(memfd);

	return 0;
}
